# vim: set cin et ts=4 sw=4 tw=80:

from PIL import Image
from io import BytesIO
import requests

class urlim:
    def __init__(self, url):
        self.r = requests.get(url)
        self.im = None
        if self.r.status_code != requests.codes.ok:
            print("Failed to access URL (status code = {:s}), using default values".format(self.r.status_code))
        else:
            try:
                self.im = Image.open(BytesIO(self.r.content))
            except:
                print("Failed to use screenshot from URL, using default width and height")

    def imdims(self):
        '''Return (width,height) for image located at URL specified
           by input string
        '''
        if self.im:
            return self.im.size
        else:
            return (0,0)

    def imfmt(self):
        '''Return image format as a string
        '''
        if self.im:
            return self.im.format
        else:
            return None

