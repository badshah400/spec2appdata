#!/usr/bin/python3
# vim: set et cin ts=4 sw=4 tw=80:

'''
Copyright (c) 2016 Atri Bhattacharya

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

import argparse
import sys
import rpm
import re
import subprocess as sp
import tempfile as tmpf
from os import path
from lxml import etree
import os
import configparser
from urlimspec import urlim

parser = argparse.ArgumentParser(description='Generate appstream with info from' 
                                             ' specfile and .desktop files for '
                                             'any given package')
parser.add_argument('pkgname', metavar='<pkg>', help='Package name')
parser.add_argument('-t', '--translatable',
                    help='Generate translatable appdata file',
                    action="store_true")
parser.add_argument('-m', '--automake',
                    help='Print the necessary automake snippet for translatable appdata file',
                    action="store_true")
parser.add_argument('-s', '--screenshot',
                    help='Use specified URL for metainfo screenshot',
                    action=None)
parser.add_argument('-d', '--dev',
                    help='Use specified contact info for metainfo update_contact',
                    action=None)
args = parser.parse_args()

pkg = args.pkgname

def findrpm(dirname):
    rpmfound = False
    for root, dirs, files in os.walk(dirname):
        for file in files:
            if file.endswith(".rpm"):
                 rpmfound = True
                 return os.path.join(root, file)
    if not rpmfound:
        sys.exit('RPM not found.')

deskkeys = ['Name', 'GenericName', 'Comment']

def getdeskdict(fname):
    res = {}
    f = open(fname, 'r')
    config = configparser.ConfigParser()
    config.read(fname)

    for v in deskkeys:
        try:
            res[v] = config['Desktop Entry'][v]
        except:
            res[v] = ''
    return res

comp = etree.Element("component")

appdict = {}

appkeys = ['id', 'metadata_license', 'name', 'project_license',
           'summary', 'url', 'description', 'screenshots', 'provides', 'update_contact']

desckeys = ['p', 'li']

if args.translatable:
    for key in ['name', 'summary']:
        i = appkeys.index(key)
        appkeys[i] = '_' + appkeys[i]
    for i, d in enumerate(desckeys):
        desckeys[i] = '_' + d

allowed_scrimg = ["PNG", "JPEG", "JPG"]
scrs = []
if args.screenshot != None:
    s = args.screenshot
    scrs = s.split(';')

for el in appkeys:
	appdict[el] = etree.SubElement(comp, el)

# Set constant values, types first
appdict['url'].set("type", "homepage")
appdict['metadata_license'].text='CC0-1.0'
appdict['update_contact'].text=(args.dev if args.dev else 'dev_AT_example.com')
appdir = 'usr/share/applications'
deskpatt = re.compile('/' + appdir + '/[a-zA-Z0-9\-\.]*.desktop')
binlocs  = re.compile('/usr/bin/[\w\W]*|/usr/sbin/[\w\W]*|/bin/[\w\W]*|/sbin/[\w\W]+')
deskfiles = []
binfiles  = []

# Use zypper to chk if pkg exists, dl pkg to tmp dir
ts = rpm.TransactionSet()

global installed
installed = False

with tmpf.TemporaryDirectory() as tmpdir:
    try:
        val = sp.check_output(['zypper' , '-qx', 'se', '-t', 'package',
                               '--match-exact', pkg])
    except:
        sys.exit('No matching pkg')
    
    pkgtree = etree.XML(val)
    eltree = etree.ElementTree(pkgtree)
    slv = eltree.find('.//solvable').get('status')

    if slv == "not-installed":
        sp.call(['zypper', '--pkg-cache-dir={:s}'.format(tmpdir), 'download', pkg])
        rpmloc = findrpm(tmpdir)
        fd     = os.open(rpmloc, os.O_RDONLY)
        h      = ts.hdrFromFdno(fd)
    else:
        installed = True
        print('Using system installed package')
        mi = ts.dbMatch('name', pkg)
        h = next(mi)        

    summ   = h.sprintf('%{Summary}').strip()
    desc   = h.sprintf('%{Description}').strip()
    url    = h.sprintf('%{URL}').strip()
    lic    = h.sprintf('%{License}').strip()
    fi     = h.fiFromHeader()

    for f in fi:
        m = deskpatt.match(f[0])
        if m:
            deskfiles.append(path.basename(m.group()))

    if not len(deskfiles):
        sys.exit('No .desktop file installed by package')

    print('\nList of .desktop file(s) installed by {:s}:'.format(pkg))    
    for i, f in enumerate(deskfiles):
        print('  {:d}. {:s}'.format(i+1, f))
    print()

    deskinfo = []

    for f in fi:
        m = binlocs.match(f[0])
        if m:
            binfiles.append(path.basename(m.group()))

    # Get curr working dir
    if not installed:
        cwd = os.getcwd()
        os.chdir(tmpdir)
        sp.call(['unrpm', rpmloc])
        for d in deskfiles:
            ddict = getdeskdict(path.join(tmpdir, appdir, d))
            deskinfo.append(ddict)
        os.chdir(cwd)
    else:
        for d in deskfiles:
            ddict = getdeskdict(path.join('/', appdir, d))
            deskinfo.append(ddict)

    appdict['url'].text  = url

    litem = []
    li = re.compile('^\s?\*', re.M)

    for mat in li.finditer(desc):
        litem.append(mat.start())

    li = re.compile('^\s?\*[\w\W]*^\*', re.M)
    partxt        = li.sub('', desc)
    li = re.compile('^\s?\*[\w\W]*\Z', re.M)
    partxt        = li.sub('', desc)

    nl = re.compile('^\s', re.MULTILINE)

    for s in nl.split(partxt):
        descp      = etree.SubElement(appdict['description'], desckeys[0])
        descp.text = s.strip('\n')
#        descp.text = re.sub('\n', ' ', descp.text)

    if len(litem):
        ul = etree.SubElement(appdict['description'], 'ul')
        for i in range(1, len(litem)):
            ulli = etree.SubElement(ul, desckeys[1])
            ulli.text = desc[litem[i-1]:litem[i]].strip('\n* ')
#            ulli.text = re.sub('\n', ' ', ulli.text)


        ulli = etree.SubElement(ul, desckeys[1])
        ulli.text = desc[litem[-1]:].strip('\n* ')
#        ulli.text = re.sub('\n', ' ', ulli.text)

    if len(scrs):
        for i,s in enumerate(scrs):
            if i:
                scr = etree.SubElement(appdict['screenshots'], 'screenshot')
            else:
                scr = etree.SubElement(appdict['screenshots'], 'screenshot',
                                       type='default')

            im = urlim(s)
            w, h = im.imdims()
            img = etree.SubElement(scr, 'image', width=str(w), height=str(h))
            img.text = '{:s}'.format(s)
            cap = etree.SubElement(scr, 'caption')
            cap.text = 'Caption for screenshot {:d}'.format(i+1)
    else:
        for i in range(0, 3):
            if i:
                scr = etree.SubElement(appdict['screenshots'], 'screenshot')
            else:
                scr = etree.SubElement(appdict['screenshots'], 'screenshot',
                                       type='default')
            img = etree.SubElement(scr, 'image', width='1600', height='900')
            img.text = 'Screenshot {:d} URL here'.format(i+1)
            cap = etree.SubElement(scr, 'caption')
            cap.text = 'Caption for screenshot {:d}'.format(i+1)

    for i,f in enumerate(binfiles):
        prv = etree.SubElement(appdict['provides'], 'binary')
        prv.text = f

    print('\nThe following appdata file(s) have been saved:')
    for i, d in enumerate(deskfiles):
        appdict['id'].text          = d
        appdict['project_license'].text = lic

        namenode = '_name' if args.translatable else 'name'
        appdict[namenode].text  = deskinfo[i]['Name']
        appdict[namenode].text += ' ' + deskinfo[i]['GenericName'] if deskinfo[i]['GenericName'] != '' else ''

        sumnode = '_summary' if args.translatable else 'summary'
        appdict[sumnode].text     = (deskinfo[i]['Comment'] 
                                       if len(deskinfo[i]['Comment']) > len(summ)
                                       else summ)

        xmlfile = path.splitext(d)[0] + '.appdata.xml'
        if args.translatable:
            xmlfile += '.in'
            trans = etree.SubElement(comp, 'translation', type='gettext')
            trans.text = pkg

        eltree = etree.ElementTree(comp)

        eltree.write(xmlfile,
                     encoding='utf-8', pretty_print=True, xml_declaration=True)
        print('  {:d}. {:s}'.format(i+1, xmlfile))

    print()

if args.automake:
    print('Makefile.am snippet to add')
    print('--------------------------')
    print('appdatadir = $(datadir)/appdata')
    if args.translatable:
        print('appdata_in_files = {:s}'.format(xmlfile))
        print('appdata_DATA = $(appdata_in_files:.appdata.xml.in=.appdata.xml)')
        print('@INTLTOOL_XML_RULE@')
        print()
        print('EXTRA_DIST += $(appdata_in_files)')
    else:
        print('appdata_DATA = {:s}'.format(xmlfile))
        print()
        print('EXTRA_DIST += {:s}'.format(xmlfile))

