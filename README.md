#Spec2appstream for openSUSE#

`spec2appstream` is a python script that makes use of openSUSE's `zypper` and RPM
to generate one/multiple appdata files by importing information from a package's
spec-file and the .desktop file(s) it installs. The generated appdata is meant
to be a template, with some fields (e.g. `screenshot`) meant to be filled in by
a maintainer/upstream/good Samaritan, and reviewed carefully.

`spec2appstream` currently accepts one package name as input, downloads the
rpm corresponding to that package in a tmp directory (using `zypper`), then
queries the RPM headers for the package and .desktop files installed by
the package for the appropriate appdata information including for the fields:

* id
* name
* summary
* description
* provides, and
* project license.

It also has optional support for specifying the following info on the command
line (see usage options below), which it then uses to populate the appropriate metainfo node:

* screenshots
* update_contact.

For other fields, including screenshots and update_contact (when not specified), it places template code that can be easily manually edited. Finally, it writes one formatted appdata.xml file for each .desktop file installed by the package.

##Usage##

`./spec2appstream <pkg_name>`

###Usage options###

The following options may be specified at the command line:

* `./spec2appstream -t <pkg_name>`: Generate gettext translatable appdata.xml.in.    
Alternate long form: `--translatable`.
* `./spec2appstream -m <pkg_name>`: Additionally print a snippet that can be added to Makefile.am in the project, can be combined with `-t`.  
Alternate long form: `--automake`.
* `./spec2appstream <pkg_name> -s "http://url1.png;http://url2.png"`: will add the images at the specified URL's (`http://url1.png` and `http://url2.png` for the example here) as screenshots, automatically determining their sizes. The specification following `-s` must be _one_ quoted string, with individual URL's separated by '`;`'.  
Alternate long form: `--screenshot="http://url1.png;http://url2.png"`.
* `./spec2appstream <pkg_name> -d "dev@email.com"`: specifies the developer's contact info, used to fill the `<update_contact>` node.  
Alternatie long form: `--dev="dev@email.com"`.

##License##
The script is distributed according to the terms and conditions of the
[MIT License](http://spdx.org/licenses/MIT.html).

##Todo##

* Improve the `<p>` vs `<ul>` logic in `<description>`.
* Build a cache dir and preserve downloaded rpm's. Download rpm's only when needed (e.g., new versions).
